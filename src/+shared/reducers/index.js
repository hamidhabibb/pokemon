import { REQUEST_CARDS, RECEIVE_CARDS, CARDS_BY_TYPE, ADD_TO_FAVORITE } from "../actions/index";

const initialState = {
  cards: [],
  favCards: []
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_CARDS:
      return { ...state, loading: true };

    case CARDS_BY_TYPE:
      return { ...state, loading: true };

    case RECEIVE_CARDS:
      const newState = action.cards;
      return { ...state, cards: newState, loading: false };

    case ADD_TO_FAVORITE:
      const favoriteNewState = action.favCards;
      return { ...state, favCards: favoriteNewState, loading: false };

    default:
      return state;
  }
};
export default rootReducer;
