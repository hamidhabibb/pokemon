import fetch from "cross-fetch";

export const REQUEST_CARDS = "REQUEST_CARDS";
export const CARDS_BY_TYPE = "CARDS_BY_TYPE";
export const RECEIVE_CARDS = "RECEIVE_CARDS";
export const ADD_TO_FAVORITE = "ADD_TO_FAVORITE";

export const addCardToFavs = id => {
  return {
    type: ADD_TO_FAVORITE,
    id
  };
};


const requestCards = pokemonName => {
  return {
    type: REQUEST_CARDS,
    pokemonName
  };
};

const requestCardsByType = pokemonType => {
  return {
    type: CARDS_BY_TYPE,
    pokemonType
  };
};

const receiveCards = (pokemonName, cards) => {
  return {
    type: RECEIVE_CARDS,
    pokemonName,
    cards: cards,
    receivedAt: Date.now()
  };
};

export const fetchCardsByType = pokemonType => {
  return dispatch => {
    dispatch(requestCardsByType(pokemonType));
    return fetch(`https://api.pokemontcg.io/v1/cards?subtype=${pokemonType}`)
      .then(res => res.json())
      .then(cards => {
        dispatch(receiveCards(pokemonType, cards));
      });
  };
};

export const fetchCards = pokemonName => {
  return dispatch => {
    dispatch(requestCards(pokemonName));
    return fetch(`https://api.pokemontcg.io/v1/cards?name=${pokemonName}`)
      .then(res => res.json())
      .then(cards => {
        dispatch(receiveCards(pokemonName, cards));
      });
  };
};
