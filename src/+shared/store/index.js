import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
// app
import rootReducer from "../reducers/index";

const store = createStore(
  rootReducer,
  composeWithDevTools(),
  applyMiddleware(thunkMiddleware)
);

export default store;
