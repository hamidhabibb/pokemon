import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
// app
import CardList from "./components/CardList";
import PokemonFilter from "./components/PokemonFilter";
import { fetchCards, fetchCardsByType } from "./+shared/actions/index";

// styles
import "./App.css";

const { Header, Content, Footer } = Layout;

class App extends Component {
  constructor(props) {
    super(props);

    this.handleSearch = this.handleSearch.bind(this);
    this.handleFilteredSearch = this.handleFilteredSearch.bind(this);
  }

  componentWillMount() {
    this.props.fetchCards("");
  }

  handleSearch(pokemonName) {
    this.props.fetchCards(pokemonName);
  }

  handleFilteredSearch(pokemonType) {
    this.props.fetchCardsByType(pokemonType);
  }

  render() {
    return (
      <Layout>
        <Header style={{ zIndex: 1, width: "100%" }}>
          <div className="logo" />
          <PokemonFilter
            handleSearch={this.handleSearch}
            filteredSearch={this.handleFilteredSearch}
          />
        </Header>
        <Content style={{ padding: "0 50px", marginTop: 64 }}>
          <div style={{ background: "#fff", padding: 24, minHeight: 380 }}>
            <CardList loading={this.props.loading} cards={this.props.results} />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Pokemon ©2019 Created by Hamid Habib
        </Footer>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return { results: state.cards, loading: state.loading };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCards: name => dispatch(fetchCards(name)),
    fetchCardsByType: type => dispatch(fetchCardsByType(type))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
