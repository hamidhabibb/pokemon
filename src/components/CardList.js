import React from "react";
// app
import Card from "./Card";
import Spinner from "./Spinner";
 
const CardList = props => (
  <div>
    <div className="card-container">
      {props.loading === false || undefined ? (
        props.cards.cards.map(card => {
          return (
            <div className="card" key={card.id}>
              <Card cardData={card} />
            </div>
          );
        })
      ) : (
        <Spinner />
      )}
    </div>
  </div>
);

export default CardList;
