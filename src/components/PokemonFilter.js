import React from "react";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { Menu } from "antd";
// app

class PokemonFilter extends React.Component {
  constructor(props) {
    super(props);
    this.handleLegendSearch = this.handleLegendSearch.bind(this);
    this.handleMegaSearch = this.handleMegaSearch.bind(this);
    this.handleGXSearch = this.handleGXSearch.bind(this);
    this.handleEXSearch = this.handleEXSearch.bind(this);
  }

  handleSubmit = e => {
    e.preventDefault();
    const search = e.target.search.value;
    this.props.handleSearch(search);
    e.target.search.value = "";
  };

  handleLegendSearch(pokemonType) {
    this.props.filteredSearch("Legend");
  }

  handleMegaSearch(pokemonType) {
    this.props.filteredSearch("MEGA");
  }

  handleGXSearch(pokemonType) {
    this.props.filteredSearch("GX");
  }

  handleEXSearch(pokemonType) {
    this.props.filteredSearch("EX");
  }

  handleKeyUp = e => {
    this.props.handleSearch(e.target.value);
  };

  render() {
    return (
      <div>
        <Menu theme="dark" mode="horizontal" style={{ lineHeight: "64px" }}>
          <Menu.Item onClick={this.handleLegendSearch} key="1">
            Legend
          </Menu.Item>
          <Menu.Item onClick={this.handleMegaSearch} key="2">
            Mega
          </Menu.Item>
          <Menu.Item onClick={this.handleGXSearch} key="3">
            GX
          </Menu.Item>
          <Menu.Item onClick={this.handleEXSearch} key="4">
            EX
          </Menu.Item>
          <Menu.Item key="5">
            My Favourite pokemons
            <Link to="/favourites"></Link>
          </Menu.Item>
        </Menu>
        <form onSubmit={this.handleSubmit}>
          <input
            className="typeahead"
            type="search"
            autoComplete="off"
            name="search"
            placeholder="Search for Pokemon"
            onKeyUp={this.handleKeyUp}
          />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { cards: state.cards };
};

export default connect(mapStateToProps)(PokemonFilter);
