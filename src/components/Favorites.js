import React from 'react';
import { connect } from "react-redux";
import store from "../+shared/store/index";

class Favourites extends React.Component {
  render() {
    const favoritePokemons = store.getState().favCards.map((card) => (
      <div key={card.id} className="fav-card">
        <img src={card.image} alt="card" />
      </div>
    ));
    
    return (
      <div className="fav-cards-wrapper">
        {store.getState().favCards.length > 0 ? (
          favoritePokemons
        ) : (
          <h1 className="add-pokemon">View your favorite pokemons here</h1>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { cards: state.cards };
};

export default connect(mapStateToProps)(Favourites);