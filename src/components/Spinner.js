import React from "react";
// app
import loader from "../+shared/images/loading.gif";

const spinner = {
  height: "10%"
};

const Spinner = props => (
  <div className="loader">
    <img style={spinner} src={loader} alt="loader" />
  </div>
);

export default Spinner;
