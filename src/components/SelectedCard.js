import React from "react";
import uuid from "uuid";
import { connect } from "react-redux";
import { Layout, Menu, Card } from "antd";
import { Button } from 'antd';
// app
import { addCardToFavs } from "../+shared/actions/index";

const { Header, Content, Footer } = Layout;
const { Meta } = Card;

class SelectedCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = props.location.state;
  }

  addToFavorite = id => {
    console.log(id);
   // push to favCard store slice
  }

  render() {
    const name = this.state.data.name;
    const img = this.state.data.imageUrlHiRes;
    const hp = this.state.data.hp;
    const evolvesFrom = this.state.data.evolvesFrom;
    const rarity = this.state.data.rarity;
    let attacks;
    let weaknesses;

    if (this.state.data.hasOwnProperty("attacks")) {
      attacks = this.state.data.attacks.map(attack => {
        return <li key={uuid()}>{attack.name}</li>;
      });
    }

    if (this.state.data.hasOwnProperty("weaknesses")) {
      weaknesses = this.state.data.weaknesses.map(weakness => {
        return <li key={uuid()}>{weakness.type}</li>;
      });
    }

    return (
      <Layout className="layout">
        <Header>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" style={{ lineHeight: "64px" }} />
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            <div className="row">
              <div className="column-left-side">
                <Card
                  hoverable
                  style={{ width: 240 }}
                  cover={<img alt="example" src={img} />}
                >
                  <Meta
                    title={name ? name : "N/A"}
                    description={hp + " HP points"}
                  />
                </Card>
              </div>
              <div className="column-right-side">
                <p>
                  <span>Rarity: </span> {rarity ? rarity : "Info not available"}
                </p>{" "}
                <br />
                <p>
                  <span>Attacks: </span>{" "}
                  {attacks ? attacks : "Info not available"}
                </p>{" "}
                <br />
                <p>
                  <span>Evolves from: </span>{" "}
                  {evolvesFrom ? evolvesFrom : "Info not available"}
                </p>{" "}
                <br />
                <p>
                  <span>Weaknesses: </span>{" "}
                  {weaknesses ? weaknesses : "Info not available"}
                </p>{" "}
                <br />
              </div>
            </div>
            <Button
              onClick={this.addToFavorite(this.state.data.id)}
              type="primary">Add to favourite
            </Button>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Pokemon ©2019 Created by Hamid Habib
        </Footer>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return { cards: state.cards, favCards: state.favCards };
};

const mapDispatchToProps = dispatch => {
  return {
    addCardToFavs: id => dispatch(addCardToFavs(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedCard);
