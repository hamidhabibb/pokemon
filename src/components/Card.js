import React from "react";
// app
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Card extends React.Component {
  render() {
    return (
      <div className="card">
        <Link
          to={{
            pathname: `/${this.props.cardData.id}`,
            state: { data: this.props.cardData }
          }}
        >
          <img src={this.props.cardData.imageUrl} alt="pokemon" />
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { cards: state.cards };
};

export default connect(mapStateToProps)(Card);
