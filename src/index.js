import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
// app
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import SelectedCard from "./components/SelectedCard";
import store from "./+shared/store/index";
import Favourites from '../src/components/Favorites';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/favourites" component={Favourites} />
        <Route path="/:id" component={SelectedCard} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
